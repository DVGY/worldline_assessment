#Read Me


- Created a repo
- Cloned it on the system
- Created a Node.js application that takes data from the form and saves in a txt file
- Pushed it to master branch
- CI/CD pipeline consists of building the app and then deploying it
- Done by creating a .yml file that makes use of stages and builds and deploys along with relics
- Same done in Jenkins but the process needs to be manually stopped
- Merged the master branch with the main branch
