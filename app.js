const express = require('express');

const app = express();

app.use(express.urlencoded());

app.get('/', function(request, response, next){

	response.send(`
		<div class="container">
			<h1 class="text-center">Submition</h1>
			<div class="card">
				<div class="card-header">Sample Form</div>
				<div class="card-body">
					<form method="POST" action="/">
						<div class="mb">
							<label>First Name</label>
							<input type="text" name="first_name" id="first_name" class="form-control" />
						</div>
		                <div class="mb">
		                	<input type="submit" name="submit_button" class="btn btn-primary" value="Add" />
		                </div>
					</form>
				</div>
			</div>
		</div>
	`);


});

app.post('/', function(request, response, next){

    var fs = require('fs')
    var stream = fs.createWriteStream("file1.txt");
    stream.write(request.body.first_name);
    stream.end()

	response.send(request.body.first_name);

});

app.listen(2000,()=>{
    console.log('Deployed at 2000')
})
